import { getNextQuestion, submitAnswer } from './quizService.js';

function createQuestion() {
  // Hilfsfunktion: Gibt Callback Funktion für Event-Listener
  // zurück, die Element, auf das geklickt wird definierte
  // Klasse hinzufügt
  function sendAndCheckAnswer(e) {
    const elFlag = e.currentTarget;
    const answer = {
      questionId: elFlag.getAttribute('data-question-id'),
      optionNrChosen: elFlag.getAttribute('data-option-nr'),
    };
    submitAnswer(answer).then((answer) => {
      const className = answer.isCorrect ? 'correct' : 'incorrect';
      elFlag.classList.add(className);
      // sicherstellen, dass nicht mehr auf die Flagge geklickt
      // werden kann
      elFlag.removeEventListener('click', sendAndCheckAnswer);
    });
  }

  // Hilfsfunktion: Erstellte Element mit Bild von
  // Flagge, auf das geklickt werden kann
  function createCountryEl(questionId, questionOption) {
    const elFlag = document.createElement('div');
    elFlag.setAttribute('class', 'flag');
    elFlag.setAttribute('data-option-nr', questionOption.optionNr);
    elFlag.setAttribute('data-question-id', questionId);
    elFlag.addEventListener('click', sendAndCheckAnswer);
    const elImg = document.createElement('img');
    elImg.setAttribute('src', questionOption.flagUrl);
    elFlag.appendChild(elImg);
    return elFlag;
  }

  getNextQuestion().then((question) => {
    // Flaggen erster Spalte hinzufügen
    const el1stCol = document.querySelector('div.col.first');
    el1stCol.innerHTML = '';
    for (let i = 0; i < 2; i++) {
      const elFlag = createCountryEl(question.id, question.options[i]);
      el1stCol.appendChild(elFlag);
    }
    // Flaggen zweiter Spalte hinzufügen
    const el2ndCol = document.querySelector('div.col.second');
    el2ndCol.innerHTML = '';
    for (let i = 2; i < 4; i++) {
      const elFlag = createCountryEl(question.id, question.options[i]);
      el2ndCol.appendChild(elFlag);
    }
    // Fragetext entsprechendem Element hinzufügen
    const elQuestion = document.getElementById('question');
    elQuestion.textContent = question.text;
  });
}

// Bei Klick auf 'Neue Frage' Button neue Frage erstellen
const elBtnNew = document.getElementById('new-question');
elBtnNew.addEventListener('click', createQuestion);

createQuestion();
