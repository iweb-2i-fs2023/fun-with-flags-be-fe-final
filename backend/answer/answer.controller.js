import { getAllByQuestion, create } from './answer.model.js';
import { getQuestion } from '../question/question.controller.js';

async function createAndCheckAnswer(request, response) {
  const optionNrChosen = parseInt(request.body.optionNrChosen);
  if (!optionNrChosen || optionNrChosen < 1 || optionNrChosen > 4) {
    return response.status(400).json({ message: 'answer wrongly formatted' });
  }
  const questionId = request.body.questionId;
  const answer = await getQuestion(questionId)
    .then(async (question) => {
      const answersSoFar = await getAllByQuestion(questionId);
      if (answersSoFar && answersSoFar.length >= 4) {
        return Promise.reject(
          `Too much answers for question with id ${questionId}`
        );
      }
      return question;
    })
    .then(async (question) => {
      // Wenn Antwort zu Frage passt, diese abspeichern
      await create(request.body);
      // Ermitteln ob Antwort korrekt war
      let isAnswerCorrect = false;
      if (question.correctOptionNr === optionNrChosen) {
        isAnswerCorrect = true;
      }
      return { isCorrect: isAnswerCorrect };
    })
    .catch((error) => {
      response.status(400);
      return { message: error };
    });
  response.json(answer);
}

export { createAndCheckAnswer };
