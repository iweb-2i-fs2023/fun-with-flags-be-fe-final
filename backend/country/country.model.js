const countries = [];

function getAll() {
  return Promise.resolve(countries);
}

function create(country) {
  const newCountry = {
    id: getNextId(),
    name: country.name,
    region: country.region,
    flagUrl: country.flagUrl,
  };
  countries.push(newCountry);
  return Promise.resolve(newCountry);
}

function getNextId() {
  if (!countries || countries.length === 0) {
    return '1';
  }
  return '' + (parseInt(countries[countries.length - 1].id) + 1);
}

export { getAll, create };
