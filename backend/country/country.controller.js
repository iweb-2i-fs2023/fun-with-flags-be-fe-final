import axios from 'axios';
import { create, getAll } from './country.model.js';

function loadCountries() {
  return axios
    .get('https://restcountries.com/v3.1/all')
    .then(async (response) => {
      const countries = response.data;
      // Geladene Informationen einzelner Länder iterieren
      for (let i = 0; i < countries.length; i++) {
        const country = countries[i];
        // Objekt mit benötigten Informationen für Quiz anlegen ...
        const newCountry = {
          name: country.translations.deu.common,
          region: country.region,
          flagUrl: country.flags.svg,
        };
        await create(newCountry);
      }
    });
}

async function getRandomCountries(nrOfCountries) {
  // Hilfsfunktion: Gibt zufällig die Indizes von vier Ländern
  // zurück
  function getRandomIndices(totalNrOfCountries) {
    let indices = [];
    while (indices.length !== 4) {
      let index = parseInt(Math.random() * totalNrOfCountries);
      if (indices.indexOf(index) === -1) {
        indices.push(index);
      }
    }
    return indices;
  }

  const countries = await getAll();
  const randomCountries = [];
  // Zufällig vier Länder auswählen
  const indices = getRandomIndices(countries.length);
  for (let i = 0; i < nrOfCountries; i++) {
    randomCountries.push(countries[indices[i]]);
  }
  return Promise.resolve(randomCountries);
}

export { loadCountries, getRandomCountries };
