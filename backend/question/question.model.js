const questions = [];

function get(id) {
  const questionsFound = questions.filter((question) => question.id === id);
  if (questionsFound && questionsFound.length === 1) {
    return Promise.resolve(questionsFound[0]);
  }
  return Promise.reject(`Failed to find question with id ${id}`);
}

function create(question) {
  const newQuestion = {
    id: getNextId(),
    text: question.text,
    options: question.options,
    correctOptionNr: question.correctOptionNr,
  };
  questions.push(newQuestion);
  return Promise.resolve(newQuestion);
}

function getNextId() {
  if (!questions || questions.length === 0) {
    return '1';
  }
  return '' + (parseInt(questions[questions.length - 1].id) + 1);
}

export { get, create };
