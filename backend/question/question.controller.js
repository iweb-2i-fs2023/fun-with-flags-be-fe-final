import { get, create } from './question.model.js';
import { getRandomCountries } from '../country/country.controller.js';

async function getNextQuestion(request, response) {
  const question = await createQuestion().then(async (question) => {
    return create(question);
  });
  response.json({
    id: question.id,
    text: question.text,
    options: question.options,
  });
}

// Frage mit 4 zufälligen Ländern erzeugen
function createQuestion() {
  return getRandomCountries(4).then((countries) => {
    // Frage Optionen (Flaggen URL) erzeugen
    const options = [];
    for (let i = 0; i < countries.length; i++) {
      const country = countries[i];
      options.push({
        optionNr: i + 1,
        flagUrl: country.flagUrl,
      });
    }
    // Zufällig richtige Antwort (Land) auswählen
    const correctIdx = parseInt(Math.random() * 4);
    const correctCountry = countries[correctIdx];
    // Länder neu sortieren, damit Reihenfolge in Fragestellung
    // nicht mit Optionen (Flaggen) übereinstimmt
    countries.sort((val1, val2) => {
      if (val1.id < val2.id) {
        return -1;
      }
      if (val1.id > val2.id) {
        return 1;
      }
      return 0;
    });
    // String mit allen ausgewählten Ländern erstellen
    let countriesStr = countries.reduce(
      (strSoFar, country, idx) =>
        strSoFar +
        country.name +
        (idx !== countries.length - 2 ? ', ' : ' und '),
      ''
    );
    // Fragetext erzeugen
    countriesStr = countriesStr.substring(0, countriesStr.length - 2);
    const text =
      `Finde unter den Flaggen von ${countriesStr} ` +
      `die Flagge von ${correctCountry.name}?`;
    return {
      text: text,
      options: options,
      correctOptionNr: correctIdx + 1,
    };
  });
}

function getQuestion(id) {
  return get(id);
}

export { getNextQuestion, getQuestion };
