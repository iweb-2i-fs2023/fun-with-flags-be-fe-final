import express from 'express';
import { loadCountries } from './backend/country/country.controller.js';
import { router as questionRouter } from './backend/question/question.routes.js';
import { router as answerRouter } from './backend/answer/answer.routes.js';

const app = express();

app.use(express.static('frontend'));

app.use(express.json());
app.use('/api/questions', questionRouter);
app.use('/api/answers', answerRouter);

await loadCountries();

app.listen(3001, async () => {
  console.log('Server listens to http://localhost:3001');
});
